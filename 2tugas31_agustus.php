<?php

class Persegi{  
    public function hitung_luas($sisi)  
    {  
    $luas=$sisi*$sisi;  
    return $luas;  
    }  

    public function hitung_keliling($sisi)  
    {  
    $keliling=$sisi*4;  
    return $keliling;  
    }  
}  

$bisa = new Persegi;  

echo "<strong>Persegi</strong>";
echo "<br>Luas = ";
echo $bisa->hitung_luas(15,15);
echo ", Keliling = " ;
echo $bisa->hitung_keliling(15,15);
echo "<br>";


class Persegi_panjang{  
    public function hitung_luas($panjang,$lebar)  
    {  
    $luas=$panjang*$lebar;  
    return $luas;  
    }  

    public function hitung_keliling($panjang,$lebar)  
    {  
    $keliling=$panjang+$lebar+$panjang+$lebar;  
    return $keliling;  
    }  
}  

$bisa = new Persegi_panjang;  

echo "<strong>Persegi Panjang</strong>";
echo "<br>Luas = ";
echo $bisa->hitung_luas(20,30);
echo ", Keliling = " ;
echo $bisa->hitung_keliling(20,30);
echo "<br>";


class Segi_tiga{  
    public function hitung_luas($alas,$tinggi)  
    {  
    $luas=$alas*$tinggi/2;  
    return $luas;  
    }  

    public function hitung_keliling($alas,$tinggi,$sisi_miring)  
    {  
    $keliling=$alas+$tinggi+$sisi_miring;  
    return $keliling;  
    }  
}  

$bisa = new Segi_tiga;  

echo "<strong>Segi Tiga</strong>";
echo "<br>Luas = ";
echo $bisa->hitung_luas(30,50);
echo ", Keliling = " ;
echo $bisa->hitung_keliling(30,50,40);
echo "<br>";

class Lingkaran{  
    public function hitung_luas($r)  
    {  
    $luas=3.14*$r*$r;  
    return $luas;  
    }  

    public function hitung_keliling($r)
    {  
    $keliling=2*3.14*$r;  
    return $keliling;  
    }  
}  

$bisa = new Lingkaran;  

echo "<strong>Lingkaran</strong>";
echo "<br>Luas = ";
echo $bisa->hitung_luas(7);
echo ", Keliling = " ;
echo $bisa->hitung_keliling(12);
echo "<br>";


class Trapesium{  
    public function hitung_luas($tinggi,$a,$b)  
    { 
    $ab = $a+$b;
    $luas=$tinggi*$ab/2;  
    return $luas;  
    }  

    public function hitung_keliling($a,$b,$c,$d)  
    {  
    $keliling=$a+$b+$c+$d;  
    return $keliling;  
    }  
}  

$bisa = new Trapesium;  

echo "<strong>Trapesium</strong>";
echo "<br>Luas = ";
echo $bisa->hitung_luas(20,12,15);
echo ", Keliling = " ;
echo $bisa->hitung_keliling(3,4,6,5);
echo "<br>";

class Jajar_genjang{  
    public function hitung_luas($alas,$tinggi)  
    {  
    $luas=$alas*$tinggi;  
    return $luas;  
    }  

    public function hitung_keliling($alas,$tinggi)  
    {  
    $at=$alas+$tinggi;
    $keliling=2*$at;  
    return $keliling;  
    }  
}  

$bisa = new Jajar_genjang;  

echo "<strong>jajar genjang</strong>";
echo "<br>Luas = ";
echo $bisa->hitung_luas(10,20);
echo ", Keliling = " ;
echo $bisa->hitung_keliling(10,20);
echo "<br>";

class Belah_ketupat{  
    public function hitung_luas($diagonal1,$diagonal2)  
    {  
    $luas=$diagonal2*$diagonal2/2;  
    return $luas;  
    }  

    public function hitung_keliling($sisi)  
    {  
    $keliling=4*$sisi;  
    return $keliling;  
    }  
}  

$bisa = new Belah_ketupat;  

echo "<strong>Belah ketupat</strong>";
echo "<br>Luas = ";
echo $bisa->hitung_luas(10,20);
echo ", Keliling = " ;
echo $bisa->hitung_keliling(10);
echo "<br>";





?>