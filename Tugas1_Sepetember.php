<?php


class CatFish{
    var $numberoflegs;
    
    var $foodclassification;
    
    var $canswim = true;
    var $respirationOrgan;
    var $canfly = false;
function eat()
{
    return 'makan plangton';
}

function run()
{
    return 'tidak bisa lari';
}

function swim()
{
    if($this->canswim==true){
        return "hewan ini bisa berenang";
    }else{
        return "hewan ini tidak bisa berenang";
    }
}

function fly()
{
    if($this->canfly==true){
        return "hewan ini bisa terbang";
    }else{
        return "hewan ini tidak bisa terbang";
    }
}

function cry()
{
    return 'tidak bisa menangis atau tidak';
}
}

$bisa = new CatFish();
$bisa -> numberoflegs = "bersirip / tidak punya kaki";
$bisa -> foodclassification = "memakan plangton";
$bisa -> respirationOrgan = "menggunakan insang";
$bisa -> canfly;
$bisa -> canswim;

echo "<strong>CatFish = </strong>";
echo $bisa->numberoflegs;
echo ", ";
echo $bisa->fly();
echo ", ";
echo $bisa->foodclassification;
echo ", ";
echo $bisa->swim();
echo ", ";
echo $bisa->respirationOrgan;
echo ", ";
echo $bisa->eat();
echo ", ";
echo $bisa->run();
echo ", ";
echo $bisa->swim();
echo ", ";
echo $bisa->fly();
echo ", ";
echo $bisa->cry();
echo ".";
echo "<br>";

class BettaFish extends CatFish{
function eat()
{
    return 'makan pelet';
}

function run()
{
    return 'tidak bisa lari';
}

function swim()
{
    if($this->canswim==true){
        return "hewan ini bisa berenang";
    }else{
        return "hewan ini tidak bisa berenang";
    }
}

function fly()
{
    if($this->canfly==true){
        return "hewan ini bisa terbang";
    }else{
        return "hewan ini tidak bisa terbang";
    }
}

function cry()
{
    return 'tidak bisa menangis atau tidak';
}
}
$cupang = new BettaFish();
$cupang -> numberoflegs = "bersirip / tidak punya kaki";
$cupang -> foodclassification = "memakan pelet";
$cupang -> respirationOrgan = "menggunakan insang";
$cupang -> canfly;
$cupang -> canswim;

echo "<strong>BettaFish = </strong>";
echo $cupang->numberoflegs;
echo ", ";
echo $cupang->fly();
echo ", ";
echo $cupang->foodclassification;
echo ", ";
echo $cupang->swim();
echo ", ";
echo $cupang->respirationOrgan;
echo ", ";
echo $cupang->eat();
echo ", ";
echo $cupang->run();
echo ", ";
echo $cupang->swim();
echo ", ";
echo $cupang->fly();
echo ", ";
echo $cupang->cry();
echo ".";
echo "<br>";

class Crocodile extends CatFish{
function eat()
{
    return 'makan daging';
}

function run()
{
    return 'bisa merayap lari';
}

function swim()
{
    if($this->canswim==true){
        return "hewan ini bisa berenang";
    }else{
        return "hewan ini tidak bisa berenang";
    }
}

function fly()
{
    if($this->canfly==true){
        return "hewan ini bisa terbang";
    }else{
        return "hewan ini tidak bisa terbang";
    }
}

function cry()
{
    return 'tidak bisa menangis atau tidak';
}
}
$buaya = new Crocodile();
$buaya -> numberoflegs = "berkaki empat";
$buaya -> foodclassification = "memakan daging";
$buaya -> respirationOrgan = "menggunakan paru paru";
$buaya -> canfly;
$buaya -> canswim;

echo "<strong>Crocodile = </strong>";
echo $buaya->numberoflegs;
echo ", ";
echo $buaya->fly();
echo ", ";
echo $buaya->foodclassification;
echo ", ";
echo $buaya->swim();
echo ", ";
echo $buaya->respirationOrgan;
echo ", ";
echo $buaya->eat();
echo ", ";
echo $buaya->run();
echo ", ";
echo $buaya->swim();
echo ", ";
echo $buaya->fly();
echo ", ";
echo $buaya->cry();
echo ".";
echo "<br>";

class Aligator extends CatFish{
function eat()
{
    return 'makan hati';
}

function run()
{
    return 'bisa lari';
}

function swim()
{
    if($this->canswim==true){
        return "hewan ini bisa berenang";
    }else{
        return "hewan ini tidak bisa berenang";
    }
}

function fly()
{
    if($this->canfly==true){
        return "hewan ini bisa terbang";
    }else{
        return "hewan ini tidak bisa terbang";
    }
}

function cry()
{
    return 'tidak bisa menangis';
}
}
$bajol = new Aligator();
$bajol -> numberoflegs = "berkaki empat";
$bajol -> foodclassification = "memakan hati";
$bajol -> respirationOrgan = "menggunakan paru paru";
$bajol -> canfly;
$bajol -> canswim;

echo "<strong>Aligator = </strong>";
echo $bajol->numberoflegs;
echo ", ";
echo $bajol->fly();
echo ", ";
echo $bajol->foodclassification;
echo ", ";
echo $bajol->swim();
echo ", ";
echo $bajol->respirationOrgan;
echo ", ";
echo $bajol->eat();
echo ", ";
echo $bajol->run();
echo ", ";
echo $bajol->swim();
echo ", ";
echo $bajol->fly();
echo ", ";
echo $bajol->cry();
echo ".";
echo "<br>";

class Lizard extends CatFish{
    
    var $canswim = false;
    
    var $canfly = false;
function eat()
{
    return 'makan danging kayaknya';
}

function run()
{
    return 'bisa lari ';
}

function swim()
{
    if($this->canswim==true){
        return "hewan ini bisa berenang";
    }else{
        return "hewan ini tidak bisa berenang";
    }
}

function fly()
{
    if($this->canfly==true){
        return "hewan ini bisa terbang";
    }else{
        return "hewan ini tidak bisa terbang";
    }
}

function cry()
{
    return 'tidak bisa menangis';
}
}
$kadal = new Lizard();
$kadal -> numberoflegs = "berkaki empat";
$kadal -> foodclassification = "memakan daging kayaknya";
$kadal -> respirationOrgan = "menggunakan paru paru";
$kadal -> canfly;
$kadal -> canswim;

echo "<strong>Lizard = </strong>";
echo $kadal->numberoflegs;
echo ", ";
echo $kadal->fly();
echo ", ";
echo $kadal->foodclassification;
echo ", ";
echo $kadal->swim();
echo ", ";
echo $kadal->respirationOrgan;
echo ", ";
echo $kadal->eat();
echo ", ";
echo $kadal->run();
echo ", ";
echo $kadal->swim();
echo ", ";
echo $kadal->fly();
echo ", ";
echo $kadal->cry();
echo ".";
echo "<br>";

class Snake extends CatFish{
function eat()
{
    return 'makan daging dan berbisa';
}

function run()
{
    return 'hewan melata';
}

function swim()
{
    if($this->canswim==true){
        return "hewan ini bisa berenang";
    }else{
        return "hewan ini tidak bisa berenang";
    }
}

function fly()
{
    if($this->canfly==true){
        return "hewan ini bisa terbang";
    }else{
        return "hewan ini tidak bisa terbang";
    }
}

function cry()
{
    return 'tidak bisa menangis';
}
}
$ular = new Snake();
$ular -> numberoflegs = "tidak punya kaki";
$ular -> foodclassification = "memakan daging";
$ular -> respirationOrgan = "menggunakan suhu";
$ular -> canfly;
$ular -> canswim;

echo "<strong>Snake = </strong>";
echo $ular->numberoflegs;
echo ", ";
echo $ular->fly();
echo ", ";
echo $ular->foodclassification;
echo ", ";
echo $ular->swim();
echo ", ";
echo $ular->respirationOrgan;
echo ", ";
echo $ular->eat();
echo ", ";
echo $ular->run();
echo ", ";
echo $ular->swim();
echo ", ";
echo $ular->fly();
echo ", ";
echo $ular->cry();
echo ".";
echo "<br>";

class Swan extends CatFish{
function eat()
{
    return 'makan ikan';
}

function run()
{
    return 'bisa lari';
}

function swim()
{
    if($this->canswim==true){
        return "hewan ini bisa berenang";
    }else{
        return "hewan ini tidak bisa berenang";
    }
}

function fly()
{
    if($this->canfly==true){
        return "hewan ini bisa terbang";
    }else{
        return "hewan ini tidak bisa terbang";
    }
}

function cry()
{
    return 'tidak bisa menangis';
}
}
$angsa = new Swan();
$angsa -> numberoflegs = "berkaki dua";
$angsa -> foodclassification = "memakan ikan";
$angsa -> respirationOrgan = "menggunakan paru paru";
$angsa -> canfly;
$angsa -> canswim;

echo "<strong>Swan = </strong>";
echo $angsa->numberoflegs;
echo ", ";
echo $angsa->fly();
echo ", ";
echo $angsa->foodclassification;
echo ", ";
echo $angsa->swim();
echo ", ";
echo $angsa->respirationOrgan;
echo ", ";
echo $angsa->eat();
echo ", ";
echo $angsa->run();
echo ", ";
echo $angsa->swim();
echo ", ";
echo $angsa->fly();
echo ", ";
echo $angsa->cry();
echo ".";
echo "<br>";

class Chicken extends CatFish{
    
    var $canswim = false;
    
    var $canfly = false;
function eat()
{
    return 'makan jagung';
}

function run()
{
    return 'bisa lari';
}

function swim()
{
    if($this->canswim==true){
        return "hewan ini bisa berenang";
    }else{
        return "hewan ini tidak bisa berenang";
    }
}

function fly()
{
    if($this->canfly==true){
        return "hewan ini bisa terbang";
    }else{
        return "hewan ini tidak bisa terbang";
    }
}

function cry()
{
    return 'tidak bisa menangis';
}
}
$ayam = new Chicken();
$ayam -> numberoflegs = "berkaki dua";
$ayam -> foodclassification = "memakan jagung";
$ayam -> respirationOrgan = "menggunakan paru paru";
$ayam -> canfly;
$ayam -> canswim;

echo "<strong>Chicken = </strong>";
echo $ayam->numberoflegs;
echo ", ";
echo $ayam->fly();
echo ", ";
echo $ayam->foodclassification;
echo ", ";
echo $ayam->swim();
echo ", ";
echo $ayam->respirationOrgan;
echo ", ";
echo $ayam->eat();
echo ", ";
echo $ayam->run();
echo ", ";
echo $ayam->swim();
echo ", ";
echo $ayam->fly();
echo ", ";
echo $ayam->cry();
echo ".";
echo "<br>";

class Swallow extends CatFish {
    
function eat()
{
    return 'makan ikan';
}

function run()
{
    return 'bisa lari';
}

function swim()
{
    if($this->canswim==true){
        return "hewan ini bisa berenang";
    }else{
        return "hewan ini tidak bisa berenang";
    }
}

function fly()
{
    if($this->canfly==true){
        return "hewan ini bisa terbang";
    }else{
        return "hewan ini tidak bisa terbang";
    }
}

function cry()
{
    return 'tidak bisa menangis atau tidak';
}
}
$mentok = new Swallow();
$mentok -> numberoflegs = "berkaki dua";
$mentok -> foodclassification = "memakan ikan";
$mentok -> respirationOrgan = "menggunakan paru paru";
$mentok -> canfly;
$mentok -> canswim;

echo "<strong>Swallow = </strong>";
echo $mentok->numberoflegs;
echo ", ";
echo $mentok->fly();
echo ", ";
echo $mentok->foodclassification;
echo ", ";
echo $mentok->swim();
echo ", ";
echo $mentok->respirationOrgan;
echo ", ";
echo $mentok->eat();
echo ", ";
echo $mentok->run();
echo ", ";
echo $mentok->swim();
echo ", ";
echo $mentok->fly();
echo ", ";
echo $mentok->cry();
echo ".";
echo "<br>";

class Duck extends CatFish{
function eat()
{
    return 'makan pelet';
}

function run()
{
    return 'bisa lari';
}

function swim()
{
    if($this->canswim==true){
        return "hewan ini bisa berenang";
    }else{
        return "hewan ini tidak bisa berenang";
    }
}

function fly()
{
    if($this->canfly==true){
        return "hewan ini bisa terbang";
    }else{
        return "hewan ini tidak bisa terbang";
    }
}

function cry()
{
    return 'tidak bisa menangis';
}
}
$bebek = new Duck();
$bebek -> numberoflegs = "berkaki dua";
$bebek -> foodclassification = "memakan pelet";
$bebek -> respirationOrgan = "menggunakan paru paru";
$bebek -> canfly;
$bebek -> canswim;

echo "<strong>Duck = </strong>";
echo $bebek->numberoflegs;
echo ", ";
echo $bebek->fly();
echo ", ";
echo $bebek->foodclassification;
echo ", ";
echo $bebek->swim();
echo ", ";
echo $bebek->respirationOrgan;
echo ", ";
echo $bebek->eat();
echo ", ";
echo $bebek->run();
echo ", ";
echo $bebek->swim();
echo ", ";
echo $bebek->fly();
echo ", ";
echo $bebek->cry();
echo ".";
echo "<br>";

class Human extends CatFish{
function eat()
{
    return 'makan segalanya';
}

function run()
{
    return 'bisa lari';
}

function swim()
{
    if($this->canswim==true){
        return "hewan ini bisa berenang";
    }else{
        return "hewan ini tidak bisa berenang";
    }
}

function fly()
{
    if($this->canfly==true){
        return "hewan ini bisa terbang";
    }else{
        return "hewan ini tidak bisa terbang";
    }
}

function cry()
{
    return 'bisa menangis atau tidak';
}
}
$cupang = new Human();
$cupang -> numberoflegs = "kaki 2";
$cupang -> foodclassification = "memakan segalanya";
$cupang -> respirationOrgan = "menggunakan paru paru";
$cupang -> canfly;
$cupang -> canswim;

echo "<strong>Human = </strong>";
echo $cupang->numberoflegs;
echo ", ";
echo $cupang->fly();
echo ", ";
echo $cupang->foodclassification;
echo ", ";
echo $cupang->swim();
echo ", ";
echo $cupang->respirationOrgan;
echo ", ";
echo $cupang->eat();
echo ", ";
echo $cupang->run();
echo ", ";
echo $cupang->swim();
echo ", ";
echo $cupang->fly();
echo ", ";
echo $cupang->cry();
echo ".";
echo "<br>";

class Whale extends CatFish{
function eat()
{
    return 'makan plangton';
}

function run()
{
    return 'tidak bisa lari';
}

function swim()
{
    if($this->canswim==true){
        return "hewan ini bisa berenang";
    }else{
        return "hewan ini tidak bisa berenang";
    }
}

function fly()
{
    if($this->canfly==true){
        return "hewan ini bisa terbang";
    }else{
        return "hewan ini tidak bisa terbang";
    }
}

function cry()
{
    return 'tidak bisa menangis atau tidak';
}
}
$cupang = new Whale();
$cupang -> numberoflegs = "bersirip / tidak punya kaki";
$cupang -> foodclassification = "memakan plangton";
$cupang -> respirationOrgan = "menggunakan paru paru";
$cupang -> canfly;
$cupang -> canswim;

echo "<strong>Whale = </strong>";
echo $cupang->numberoflegs;
echo ", ";
echo $cupang->fly();
echo ", ";
echo $cupang->foodclassification;
echo ", ";
echo $cupang->swim();
echo ", ";
echo $cupang->respirationOrgan;
echo ", ";
echo $cupang->eat();
echo ", ";
echo $cupang->run();
echo ", ";
echo $cupang->swim();
echo ", ";
echo $cupang->fly();
echo ", ";
echo $cupang->cry();
echo ".";
echo "<br>";

class Dholpine extends CatFish{
function eat()
{
    return 'makan ikan';
}

function run()
{
    return 'tidak bisa lari';
}

function swim()
{
    if($this->canswim==true){
        return "hewan ini bisa berenang";
    }else{
        return "hewan ini tidak bisa berenang";
    }
}

function fly()
{
    if($this->canfly==true){
        return "hewan ini bisa terbang";
    }else{
        return "hewan ini tidak bisa terbang";
    }
}

function cry()
{
    return 'tidak bisa menangis atau tidak';
}
}
$cupang = new Dholpine();
$cupang -> numberoflegs = "bersirip / tidak punya kaki";
$cupang -> foodclassification = "memakan ikan";
$cupang -> respirationOrgan = "menggunakan paru paru";
$cupang -> canfly;
$cupang -> canswim;

echo "<strong>Dholpine = </strong>";
echo $cupang->numberoflegs;
echo ", ";
echo $cupang->fly();
echo ", ";
echo $cupang->foodclassification;
echo ", ";
echo $cupang->swim();
echo ", ";
echo $cupang->respirationOrgan;
echo ", ";
echo $cupang->eat();
echo ", ";
echo $cupang->run();
echo ", ";
echo $cupang->swim();
echo ", ";
echo $cupang->fly();
echo ", ";
echo $cupang->cry();
echo ".";
echo "<br>";

class Bat extends CatFish{
    
    var $canswim = false;
    var $canfly = true;
function eat()
{
    return 'makan buah';
}

function run()
{
    return 'tidak bisa lari';
}

function swim()
{
    if($this->canswim==true){
        return "hewan ini bisa berenang";
    }else{
        return "hewan ini tidak bisa berenang";
    }
}

function fly()
{
    if($this->canfly==true){
        return "hewan ini bisa terbang";
    }else{
        return "hewan ini tidak bisa terbang";
    }
}

function cry()
{
    return 'tidak bisa menangis';
}
}
$cupang = new Bat();
$cupang -> numberoflegs = "berkaki dua";
$cupang -> foodclassification = "memakan buah";
$cupang -> respirationOrgan = "menggunakan paru paru";
$cupang -> canfly;
$cupang -> canswim;

echo "<strong>Bat = </strong>";
echo $cupang->numberoflegs;
echo ", ";
echo $cupang->fly();
echo ", ";
echo $cupang->foodclassification;
echo ", ";
echo $cupang->swim();
echo ", ";
echo $cupang->respirationOrgan;
echo ", ";
echo $cupang->eat();
echo ", ";
echo $cupang->run();
echo ", ";
echo $cupang->swim();
echo ", ";
echo $cupang->fly();
echo ", ";
echo $cupang->cry();
echo ".";
echo "<br>";

class Tiger extends CatFish{
function eat()
{
    return 'makan daging';
}

function run()
{
    return 'larinya kencang sekali';
}

function swim()
{
    if($this->canswim==true){
        return "hewan ini bisa berenang";
    }else{
        return "hewan ini tidak bisa berenang";
    }
}

function fly()
{
    if($this->canfly==true){
        return "hewan ini bisa terbang";
    }else{
        return "hewan ini tidak bisa terbang";
    }
}

function cry()
{
    return 'tidak bisa menangis';
}
}
$cupang = new Tiger();
$cupang -> numberoflegs = "berkaki empat";
$cupang -> foodclassification = "memakan daging";
$cupang -> respirationOrgan = "menggunakan paru paru";
$cupang -> canfly;
$cupang -> canswim;

echo "<strong>Tiger = </strong>";
echo $cupang->numberoflegs;
echo ", ";
echo $cupang->fly();
echo ", ";
echo $cupang->foodclassification;
echo ", ";
echo $cupang->swim();
echo ", ";
echo $cupang->respirationOrgan;
echo ", ";
echo $cupang->eat();
echo ", ";
echo $cupang->run();
echo ", ";
echo $cupang->swim();
echo ", ";
echo $cupang->fly();
echo ", ";
echo $cupang->cry();
echo ".";
echo "<br>";
?>

